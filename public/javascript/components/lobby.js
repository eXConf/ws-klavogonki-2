import { Room } from "./room.js";
import { Race } from "./race.js";
import { createElement } from "../helpers/domHelper.mjs";

const events = {
  ROOMS_LIST: "ROOMS_LIST",
  CREATE_ROOM: "CREATE_ROOM",
  JOINED_ROOM: "JOINED_ROOM",
};

export class Lobby {
  constructor(username) {
    this.username = username;
    this.socket = null;
    this.rooms = [];
    this.roomsPage = document.getElementById("rooms-page");
  }

  init() {
    this.socket = io("", { query: { username: this.username } });
    this.subscribe();
  }

  subscribe() {
    const { socket } = this;

    socket.on(events.ROOMS_LIST, rooms => this.onRoomsList(rooms));
    socket.on(events.JOINED_ROOM, room => this.onJoinedRoom(room));
  }

  onRoomsList(rooms) {
    this.rooms = [];

    rooms.forEach(room => {
      this.rooms.push(new Room(room, this.socket));
    });
    this.render();
  }

  onJoinedRoom({ name, players }) {
    const race = new Race({
      username: this.username,
      roomName: name,
      players,
      socket: this.socket,
    });
    race.render();
  }

  render() {
    this.roomsPage.innerHTML = "";
    this.rooms.forEach(room => {
      room.render();
    });
    this.renderCreateRoomButton();
  }

  renderCreateRoomButton() {
    const newRoomBtn = createElement({
      tagName: "button",
      className: "add-room-btn btn",
    });

    newRoomBtn.innerText = "Create Room";
    newRoomBtn.addEventListener("click", () => {
      const newRoomName = prompt("Please enter a name for the new room");
      if (!newRoomName) return;

      this.socket.emit(events.CREATE_ROOM, newRoomName);
    });

    this.roomsPage.appendChild(newRoomBtn);
  }
}
