const events = {
  USER_LEFT_ROOM: "USER_LEFT_ROOM",
};

export class RoomInfo {
  constructor({ username, roomName, socket }) {
    this.username = username;
    this.roomName = roomName;
    this.socket = socket;

    this.gamePage = document.getElementById("game-page");
    this.roomsPage = document.getElementById("rooms-page");
    this.roomNameDiv = document.getElementById("room-name");
    this.quitRoomButton = document.getElementById("quit-room-btn");
  }

  render() {
    this.roomNameDiv.innerText = this.roomName;
    this.quitRoomButton.replaceWith(this.quitRoomButton.cloneNode(true));
    this.quitRoomButton = document.getElementById("quit-room-btn");
    this.quitRoomButton.addEventListener("click", () => {
      this.socket.emit(events.USER_LEFT_ROOM, this.username);
      this.exitRoom();
    });
    this.enterRoom();
  }

  enterRoom() {
    this.hideLobby();
    this.showGame();
  }

  exitRoom() {
    this.hideGame();
    this.showLobby();
  }

  showGame() {
    this.gamePage.classList.remove("display-none");
  }

  hideGame() {
    this.gamePage.classList.add("display-none");
  }

  showLobby() {
    this.roomsPage.classList.remove("display-none");
  }

  hideLobby() {
    this.roomsPage.classList.add("display-none");
  }
}
