const events = {
  PLAYER_READY: "PLAYER_READY",
  PLAYER_NOT_READY: "PLAYER_NOT_READY",
  GAME_STARTED: "GAME_STARTED",
};

export class GameField {
  constructor({ roomName, socket }) {
    this.isReady = false;
    this.isStarted = false;
    this.timeToStart = null;
    this.timeToGameEnd = null;
    this.textNotTyped = "";
    this.textCurrent = "";
    this.textTyped = "";
    this.roomName = roomName;
    this.socket = socket;

    this.socket.on(events.GAME_STARTED, textId => this.onGameStarted(textId));
  }

  render() {
    this.renderReadyButton();
    this.renderText();
    this.renderTimeToStart();
    this.renderTimeToGameEnd();
  }

  renderReadyButton() {
    let button = document.getElementById("ready-btn");
    button.replaceWith(button.cloneNode(true));
    button = document.getElementById("ready-btn");
    if (this.isStarted) {
      button.classList.add("display-none");
      return;
    }
    button.classList.remove("display-none");
    button.innerText = this.isReady ? "NOT READY" : "READY";
    button.addEventListener("click", () => this.onReadyButtonClicked());
  }

  onReadyButtonClicked() {
    if (this.isReady) {
      return this.socket.emit(events.PLAYER_NOT_READY, this.roomName);
    }
    this.socket.emit(events.PLAYER_READY, this.roomName);
  }

  onGameStarted(textId) {
    this.isStarted = true;
    this.getText(textId);
    this.render();
    this.runTimerToStart();
  }

  renderText() {
    if (!this.timeToGameEnd) return;

    this.renderTextNotTyped();
    this.renderTextCurrent();
    this.renderTextTyped();
  }

  renderTimeToStart() {
    const timeToStartDiv = document.getElementById("timer");
    timeToStartDiv.innerText = this.timeToStart || "";
  }

  renderTimeToGameEnd() {
    const timeToGameEndDiv = document.getElementById("text-timer");
    timeToGameEndDiv.innerText = this.timeToGameEnd || "";
  }

  renderTextNotTyped() {
    const textNotTypedSpan = document.getElementById("text-not-typed");
    textNotTypedSpan.innerText = this.textNotTyped;
  }

  renderTextCurrent() {
    const textCurrentSpan = document.getElementById("text-current");
    textCurrentSpan.innerText = this.textCurrent;
  }

  renderTextTyped() {
    const textTypedSpan = document.getElementById("text-typed");
    textTypedSpan.innerText = this.textCurrent;
  }

  runTimerToStart() {
    this.timeToStart = 10;

    const timer = setInterval(() => {
      this.decreaseTimeToStart(timer);
    }, 1000);
  }

  decreaseTimeToStart(timer) {
    this.timeToStart -= 1;

    if (this.timeToStart < 0) {
      clearInterval(timer);
      this.timeToStart = null;
    }

    this.render();
  }

  runTimerToGameEnd() {
    this.timeToGameEnd = 60;

    const timer = setInterval(() => {
      this.decreaseTimeToGameEnd(timer);
    }, 1000);
  }

  decreaseTimeToGameEnd(timer) {
    this.timeToGameEnd -= 1;

    if (this.timeToGameEnd <= 0) {
      clearInterval(timer);
      this.onGameEnd();
    }
  }

  onGameEnd() {}

  async getText(textId) {
    try {
      const response = await fetch(`/game/texts/${textId}`);
      const { text } = await response.json();
      this.textNotTyped = text;
    } catch {
      alert("Failed to receive text");
      console.error("Failed to receive text");
    }
  }
}
