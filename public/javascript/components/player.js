import { createElement } from "../helpers/domHelper.mjs";

export class Player {
  constructor({ name, isReady, progress }) {
    this.name = name;
    this.isReady = isReady;
    this.progress = progress;
    this.list = document.getElementById("players-list");
  }

  render() {
    const player = this.createPlayerDiv();
    this.list.appendChild(player);
  }

  createPlayerDiv() {
    const playerWrapper = createElement({
      tagName: "div",
      className: "player-wrapper",
    });

    const nameWrapper = createElement({
      tagName: "div",
      className: "name-wrapper flex-left-centered flex-row",
    });

    const status = createElement({
      tagName: "div",
      className: `ready-status ${
        this.isReady ? "ready-status-green" : "ready-status-red"
      }`,
    });

    const name = createElement({
      tagName: "div",
      className: "player-name",
    });
    name.innerText = this.name;

    const progressbarWrapper = createElement({
      tagName: "div",
      className: "progressbar-wrapper",
    });

    const progressbar = createElement({
      tagName: "div",
      className: `user-progress ${this.name}`,
    });
    progressbar.style.width = `${this.progress}%`;

    playerWrapper.appendChild(nameWrapper);
    nameWrapper.appendChild(status);
    nameWrapper.appendChild(name);
    playerWrapper.appendChild(progressbarWrapper);
    progressbarWrapper.appendChild(progressbar);

    return playerWrapper;
  }
}
