import { createElement } from "../helpers/domHelper.mjs";

const events = {
  JOIN_ROOM: "JOIN_ROOM",
};

export class Room {
  constructor(room, socket) {
    this.socket = socket;
    this.name = room.name;
    this.playersCount = room.playersCount;

    this.roomsPage = document.getElementById("rooms-page");
  }

  render() {
    const room = this.createRoom();
    this.roomsPage.appendChild(room);
  }

  createRoom() {
    const wrapperDiv = this.createWrapperDiv();
    const infoDiv = this.createInfoDiv();
    const joinBtn = this.createJoinBtn();

    wrapperDiv.appendChild(infoDiv);
    wrapperDiv.appendChild(joinBtn);

    return wrapperDiv;
  }

  createWrapperDiv() {
    const wrapper = createElement({
      tagName: "div",
      className: "room flex-centered flex-column",
    });

    return wrapper;
  }

  createInfoDiv() {
    const info = createElement({
      tagName: "div",
      className: "room-info",
    });

    info.innerText = `${this.name}: ${this.playersCount} player(s)`;

    return info;
  }

  createJoinBtn() {
    const button = createElement({
      tagName: "button",
      className: "join-btn btn",
    });

    button.innerText = "Join";
    button.addEventListener("click", () => {
      this.socket.emit(events.JOIN_ROOM, this.name);
    });

    return button;
  }
}
