import { RoomInfo } from "./roomInfo.js";
import { Player } from "./player.js";
import { GameField } from "./gameField.js";

const events = {
  PLAYERS_UPDATE: "PLAYERS_UPDATE",
  GAME_STARTED: "GAME_STARTED",
};

export class Race {
  constructor({ username, roomName, players, socket }) {
    this.username = username;
    this.roomName = roomName;
    this.players = players;
    this.text = "";
    this.isStarted = false;
    this.socket = socket;

    this.socket.on(events.PLAYERS_UPDATE, players =>
      this.onPlayersUpdate(players)
    );
  }

  onPlayersUpdate(players) {
    this.players = [...players];
    this.renderPlayers();
    this.renderGameField();
  }

  render() {
    this.renderRoomInfo();
    this.renderPlayers();
    this.renderGameField();
  }

  renderRoomInfo() {
    const roomInfo = new RoomInfo({
      username: this.username,
      roomName: this.roomName,
      socket: this.socket,
    });
    roomInfo.render();
  }

  renderPlayers() {
    const list = document.getElementById("players-list");
    list.innerHTML = "";

    this.players.forEach(info => {
      const player = new Player(info);
      player.render();
    });
  }

  renderGameField() {
    const player = this.players.find(el => el.name === this.username);
    const gameField = new GameField({
      isReady: player.isReady,
      isStarted: this.isStarted,
      text: this.text,
      roomName: this.roomName,
      socket: this.socket,
    });

    gameField.render();
  }
}
