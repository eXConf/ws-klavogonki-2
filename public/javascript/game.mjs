import { Lobby } from "./components/lobby.js";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const lobby = new Lobby(username);
lobby.init();
